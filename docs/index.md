---
# https://vitepress.dev/reference/default-theme-home-page
layout: home
title: AI-Powered Editor

hero:
  name: "AIEditor"
  text: a next-gen rich text editor for AI
  tagline: Out-of-the-box, Fully Framework Supported, Markdown Friendly
  actions:
    - theme: brand
      text: Quick Start
      link: /getting-started
    - theme: alt
      text: Live Preview
      link: /demo



#features:
#  - title: Lightweight
#    details: AiEditor is ready to use out of the box, Development based on Web Components, and does not depend on any rendering framework such as VUE, React or Angular, making it compatible with almost any framework.
#  - title: Intelligent
#    details: AiEditor supports AI continuation, AI optimization, AI proofreading, AI translation, and custom AI menus with their respective Prompts. It supports integration with models like ChatGPT, Spark and private LLMs.
#  - title: Powerful
#    details: In addition to basic functions, AiEditor also supports features that many top editors do not have, such as format painting, merging and unmerging of cells, light and dark themes, mobile adaptation, and more.
---


<style>
.VPContent> .VPHome> .container{
    width: 100% !important;
    padding: 0 !important;
    margin: 0 !important;
    max-width: 100%;
}
</style>

<style scoped>

.VPHome svg{
    width: 24px;
    display: inline-block;
    margin: 0 5px;
}

.VPContent> .VPHome {
    margin-bottom: 0;
}

.VPContent> .VPHome> .container .feature{
   text-align: center;
   margin: 40px;
}

.VPContent> .VPHome> .container .feature p{
   color: #999;
}

.VPContent> .VPHome> .vp-doc  table{
    display: inline-block;
    background: none;
    border-collapse: separate;
    border-spacing: 30px 10px;
    max-width:980px;
}

.VPContent> .VPHome> .vp-doc  table th{
     background: none;
     border: none;
}

.VPContent> .VPHome> .vp-doc  thead tr :not(:first-child){
     border-bottom: solid 1px #ddd;
     margin: 10px;
     font-weight: bold;
     font-size: 16px;
}

.VPContent> .VPHome> .vp-doc  table tr{
     background: none;
     border: none;
}

.VPContent> .VPHome> .vp-doc  table tr{
     height: 40px;
}

.VPContent> .VPHome> .vp-doc  table  tbody tr:first-child{
     height: 20px;
}

.VPContent> .VPHome> .vp-doc  table td{
    background: none;
    border: none;
}

.VPContent> .VPHome> .vp-doc  table td svg{
    margin: -7px 0;
}

.VPContent> .VPHome> .vp-doc table td:nth-of-type(1){
    color: #999;
}


.VPContent> .VPHome> .vp-doc table td:nth-of-type(2) svg{
    fill: #8C8C8C;
    margin-right:10px;
    width: 20px;
    margin:-4px 0;
   
    /* padding: 0px; */
}

.VPContent> .VPHome> .vp-doc table td:nth-of-type(3) svg{
    fill: #646cff;
}
.feature-content{
    width: 50%;    
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding: 0 50px;
}

.feature-content>h1{
  margin-bottom:30px;
}

.feature-content>p{
  color:#666;
}


</style>

<div style="text-align: center;background-color: #f8f9fa;padding: 80px">

# Why choose AiEditor

<div style="margin: 30px 0 40px;color: #999">


Simple, easy to use, open source licence friendly, no limit count of users and apps, rich documentation.

</div>

|                                         | Other editors                              | AIEditor                                                                                                                         |
|-----------------------------------------|:-------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------|
|                                         |
| Open source agreement                   | <Unhappy /> GPL, contagious                | <Check /> LGPL, not contagious when installed via NPM                                                                            |
| Out of the box                          | <Unhappy /> Complex and troublesome to use | <Check /> Can be used via `npm install aieditor`                                                                                 |
| Framework support                       | <Unhappy /> Specific few frameworks        | <Check /> AIEditor is developed based on Web Component and supports any framework                                                |
| Server side supports private deployment | <Unhappy /> Not supported                  | <Check /> Both the open source version and the commercial version support private deployment to ensure data and privacy security |
| LLMs type                               | <Unhappy /> few support                    | <Check /> Supports access to LLMs, including private LLM                                                                         |
| Private Apikey                          | <Unhappy /> Not supported                  | <Check /> Support, ensure data privacy and control your own consumption                                                          |
| Limit count of users                    | <Unhappy /> Limit                          | <Check /> Unlimited                                                                                                           |
| Limit count of apps                     | <Unhappy /> Limit                          | <Check /> Unlimited                                                                                                          |

</div>



<div class="feature">

#  Features


An AI-powered rich text editor,<br/>
Help you build knowledge products quickly.

</div>


<div style="display: flex;justify-content: center">
<div style="display: flex;padding: 20px 0;max-width: 1280px">

<div style="width: 50%">

![](/assets/image/install-en.png)

</div>

<div class="feature-content">

<h1>Out of the box</h1>

Without a lot of preparation,Just a few lines of code to run.

</div>
</div>
</div>





<div style="display: flex;justify-content: center">
<div style="display: flex;padding: 20px 0;max-width: 1280px">



<div class="feature-content">

<h1>Markdown Friendly</h1>

Be able to recognize and correctly render the basic syntax of Markdown,
see results in real time.

</div>

<div style="width: 50%">

![](/assets/image/markdown-en.png)

</div>

</div>
</div>



<div style="display: flex;justify-content: center">
<div style="display: flex;padding: 20px 0;max-width: 1280px">

<div style="width: 50%">

![](/assets/image/ai-en.png)

</div>

<div class="feature-content">

<h1>Powerful AI capabilities</h1>

You can have AI check for spelling and grammar errors;<br/>
Can expand 10 words to 200 words;<br/>
Or condense 200 words into 50 words;<br/>
You can One-click translation;<br/>
Can let the AI summarize the core content...

All this is done without leaving the editor.

More AI capabilities are on the way...

</div>
</div>
</div>





<div style="display: flex;justify-content: center">
<div style="display: flex;padding: 20px 0;max-width: 1280px">



<div class="feature-content">

<h1>Collaboration</h1>

Allows multiple users to work on the same document at the same time. <br/>
You can see other people's input and changes in real time.

It is suitable for software development, marketing, legal document review, project management, teaching, news reporting, medical research and other scenarios.

</div>

<div style="width: 50%">

![](/assets/image/feature1-en.png)

</div>

</div>
</div>

<div style="display: flex;justify-content: center">
<div style="display: flex;padding: 20px 0;max-width: 1280px">

<div style="width: 50%">

![](/assets/image/comment-en.png)

</div>

<div class="feature-content">

<h1> Comment</h1>

Allow reviewers to add comments or suggestions to specific sections of the document, as well as flag questions, errors, or areas for improvement in the document for subsequent revision.

</div>
</div>
</div>






<div class="feature">

#  Integrate excellent products

We integrate great open source works into AiEditor, such as virtual whiteboard, hand-drawn sketches, chart editor, and more.

</div>

<div style="display: flex;justify-content: center">
<div style="display: flex;padding: 20px 0;max-width: 1280px">

<div style="width: 46%;background: #f8f9fa;margin: 2%; padding:30px;border-radius: 15px;margin-right: 15px;">
<span style="font-weight:700;font-size: 24px;">drawio</span>
<div style="font-size: 16px;color:#666;height: 60px;padding-top: 10px">
draw.io is a JavaScript, client-side editor for general diagramming.
</div>
<img src="/assets/image/drawio.jpg" />
</div>


<div style="width: 46%;background: #f8f9fa;margin:  2%; padding:30px;border-radius: 15px;margin-right: 15px;">
<span style="font-weight:700;font-size: 24px">excalidraw</span>
<div style="font-size: 16px;color:#666;height: 60px;padding-top: 10px;">
Virtual whiteboard for sketching hand-drawn like diagrams.
</div>
<img src="/assets/image/excalidraw.jpg" />
</div>



</div>
</div>





<div style="text-align: center;background-color: #f8f9fa;padding: 80px">

# Are you ready?

<div style="margin: 30px 0 40px;color: #999">
Next, whether you use the open source version or the pro version, you will be surprised!
</div>

<div style="display: flex;justify-content: center">
<div style="display: flex;padding: 20px 0;max-width: 1280px">

<div style="width: 440px;background: #eeeff0;padding: 20px;border-radius: 5px;margin-right: 15px;">
<span style="font-weight:700;">open source version</span><br />

<a href="https://github.com/aieditor-team/aieditor" target="_blank" style="background: #1b1b1f;color: #fff;padding: 10px 50px;border-radius: 5px;font-weight: bold;font-size: 14px;margin: 20px 0 40px 0;text-decoration:none;display:inline-block">Download now</a>
<div style="font-size: 14px;color:#666;">
AiEditor is open source under the more liberal LGPL license<br />
Unlimited amount of users<br />
Unlimited amount of applications
</div>
</div>


<div style="width: 440px;background: #eeeff0;padding: 20px;border-radius: 5px;margin-left: 15px">
<span style="font-weight:700;">pro version</span><br />

<a href="contact-us" target="_blank" style="background: #1b1b1f;color: #fff;padding: 10px 50px;border-radius: 5px;font-weight: bold;font-size: 14px;margin: 20px 0 40px 0;text-decoration:none;display:inline-block">Contact us</a>
<div style="font-size: 14px;color:#666;">
Low price<br />
Unlimited amount of users<br />
Unlimited amount of applications
</div>
</div>


</div>
</div>
</div>

